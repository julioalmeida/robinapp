//
//  CustomTextField.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 05/10/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import UIKit

class CustomTextField: UITextField, UITextFieldDelegate {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    let border = CALayer()
    let width = CGFloat(2.0)
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.delegate=self;
        
        border.borderColor = UIColor( red: 32.0/255, green: 53.0/255, blue:88.0/255, alpha: 1.0 ).cgColor
        
        border.frame = CGRect(x: 0, y: self.frame.size.height - width, width:  self.frame.size.width, height: self.frame.size.height)
        border.borderWidth = width
        
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        self.layer.addSublayer(border)
        self.layer.masksToBounds = true
        
    }
    
    
    func textRectForBounds(bounds: CGRect) -> CGRect {
        return super.textRect(forBounds: UIEdgeInsetsInsetRect(bounds, UIEdgeInsetsMake(0, 15, 0, 0)))
    }
    
    func editingRectForBounds(bounds: CGRect) -> CGRect {
        return super.editingRect(forBounds: UIEdgeInsetsInsetRect(bounds,  UIEdgeInsetsMake(0, 15, 0, 0)))
    }


}
