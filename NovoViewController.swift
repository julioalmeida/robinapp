//
//  NovoViewController.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 10/1/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import UIKit
import Firebase
import GoogleMaps

class NovoViewController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {


    @IBOutlet weak var gmView: GMSMapView!
    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var txtDescription: UITextField!
    @IBOutlet weak var category: UIPickerView!

    var locationManager: CLLocationManager!
    var pinLatidude:CLLocationDegrees = 0.00
    var pinLongitude:CLLocationDegrees = 0.00
    let categories = ["Perigo / Violência", "Problemas de saneamento", "Evento / Festa", "Desastres naturais", " Acidente", "Obras", "Problemas elétricos", "Outros"]
    var categoriePin:String = "perigo"
    
    var ref: FIRDatabaseReference!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        configureDatabase()
        configureMap()
        gmView.delegate = self
        category.dataSource = self
        category.delegate = self
        
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return categories[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        switch row{
            case 0:
                categoriePin = "perigo"
            break
            case 1:
                categoriePin = "agua"
            break
            case 2:
                categoriePin = "evento"
            break
            case 3:
                categoriePin = "incendio"
            break
            case 4:
                categoriePin = "construcao"
            break
            case 5:
                categoriePin = "energia"
            break
            default:
                categoriePin = "default"
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.gmView.isMyLocationEnabled = true
        if let location = locations.first {
            
            gmView.clear()
            
            pinLatidude = location.coordinate.latitude
            pinLongitude = location.coordinate.longitude
            
            let  position = CLLocationCoordinate2DMake(location.coordinate.latitude, location.coordinate.longitude)
            let marker = GMSMarker(position: position)
            marker.title = ""
            marker.map = gmView
            
            self.gmView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
        }
        locationManager.stopUpdatingLocation()
        
    }
    
    func configureDatabase() {
        ref = FIRDatabase.database().reference()
    }
    
    func configureMap(){
        self.gmView.settings.myLocationButton = true
    }
    
    // MARK: GMSMapViewDelegate
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        

        gmView.clear()
        
        pinLatidude = coordinate.latitude
        pinLongitude = coordinate.longitude

        let  position = CLLocationCoordinate2DMake(coordinate.latitude, coordinate.longitude)
        let marker = GMSMarker(position: position)
        marker.title = ""
        marker.map = gmView
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func newPin(_ sender: AnyObject) {
        
        let title = txtTitle.text!
        let description = txtDescription.text!
        let uid = AppState.sharedInstance.uid!
        let name = AppState.sharedInstance.displayName!
        let date = FIRServerValue.timestamp()
        
        let data = [
            "titulo": title,
            "descricao": description,
            "longitude": pinLongitude,
            "latitude": pinLatidude,
            "categoria": categoriePin,
            "data": date
        ] as [String : Any]
        
        let userdata:[String: AnyObject] = ["id": uid as AnyObject, "nome": name as AnyObject]
        
        let key = self.ref.child("pins").childByAutoId().key
        self.ref.child("pins").child(key).setValue(data)
        self.ref.child("pins").child(key).child("usuario").setValue(userdata)
        
        let geoFireRef = GeoFire(firebaseRef: ref.child("locations"))
        
        geoFireRef?.setLocation(CLLocation(latitude: pinLatidude, longitude: pinLongitude), forKey: key)
        
        let alert = UIAlertController(title: "Cadastro", message: "Cadastro realizado com sucesso!", preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
            self.dismiss(animated: true, completion: {});
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func cancel(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: {});
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
