//
//  AppState.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 9/29/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import Foundation

class AppState: NSObject {
    
    static let sharedInstance = AppState()
    
    var signedIn = false
    var displayName: String?
    var raio: Int?
    var uid: String?
}
