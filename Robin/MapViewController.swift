//
//  MapViewController.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 10/1/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import Firebase
import CoreLocation

class MapViewController: UIViewController, CLLocationManagerDelegate {
    

    @IBOutlet weak var gmView: GMSMapView!
    @IBOutlet weak var btnView: UIView!
    
    var ref: FIRDatabaseReference!
    var locationManager = CLLocationManager()
    
    var resultsViewController: GMSAutocompleteResultsViewController?
    var searchController: UISearchController?
    var resultView: UITextView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        resultsViewController = GMSAutocompleteResultsViewController()
        resultsViewController?.delegate = self
        
        searchController = UISearchController(searchResultsController: resultsViewController)
        searchController?.searchResultsUpdater = resultsViewController
        
        let subView = UIView(frame: CGRect(x:0, y:0, width:350.0, height:45.0))
        
        subView.addSubview((searchController?.searchBar)!)
        self.gmView.addSubview(subView)
//        searchController?.searchBar.sizeToFit()
        searchController?.hidesNavigationBarDuringPresentation = false
        
        // When UISearchController presents the results view, present it in
        // this view controller, not one further up the chain.
        self.definesPresentationContext = true
        
        
        self.hideKeyboardWhenTappedAround() 

        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
        let initialLocation = CLLocation(latitude: -23.574761, longitude: -46.622472)
        configureMap(initialLocation)
        
        let button = UIButton()
        let buttonImage = UIImage(named: "add-button")
        let bottom = (self.view.frame.height-120)
        print(bottom)
        button.setImage(buttonImage, for: .normal)
        button.frame = CGRect(x: 10, y: bottom, width: 60, height: 60)
        button.addTarget(self, action: "newPin", for: .touchUpInside)
        self.gmView.addSubview(button)
        
    }
    
    func newPin(){
        performSegue(withIdentifier: "addSegue", sender: self)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

            self.gmView.isMyLocationEnabled = true
            if let location = locations.first {
                self.gmView.camera = GMSCameraPosition(target: location.coordinate, zoom: 15, bearing: 0, viewingAngle: 0)
                let location = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
                configureMap(location)
            }
            locationManager.stopUpdatingLocation()
        
    }
    
    func configureMap(_ location:CLLocation){
        gmView.clear()
        self.gmView.settings.myLocationButton = true
        
        ref = FIRDatabase.database().reference()
        let geoFireRef = GeoFire(firebaseRef: ref.child("locations"))
        
        let center = location
        let circleQuery = geoFireRef?.query(at: center, withRadius: 5)
        
        circleQuery?.observe(.keyEntered, with: { (key: String?, location: CLLocation?) in
            self.ref.child("pins").child(key!).observeSingleEvent(of: .value, with: { snapshot in
                if let value = snapshot.value as? [String : AnyObject]{
                    self.addMarker(value)
                }
            })
        })
    }
    
    func addMarker(_ val: [String : AnyObject]){
        let lat = val["latitude"] as! CLLocationDegrees
        let lon = val["longitude"] as! CLLocationDegrees
        let title = val["titulo"] as! String
        var description = val["descricao"] as! String
        var icon = "pin_default"
        
        if let user = val["usuario"] as AnyObject?{
            var fullname = ""
            if let first = user["nome"] as! String?{
                fullname = first
                if let last = user["sobrenome"] as! String?{
                    fullname = fullname + " " + last
                }
            }
            description = description+"\n"+fullname
        }
        
        if let timestamp = val["data"] as? TimeInterval{
            let timestampok = timestamp/1000
            let date = Date(timeIntervalSince1970: timestampok)
            description = description + ": "+timeAgoSince(date)
        }
        
        if let category = val["categoria"] as? String{
            icon = "pin_"+category
        }
        
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2DMake(lat, lon)
        marker.title = title
        marker.snippet = description
        marker.icon = UIImage(named: icon)
        marker.map = self.gmView
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MapViewController: GMSAutocompleteResultsViewControllerDelegate{
    /**
     * Called when a non-retryable error occurred when retrieving autocomplete predictions or place
     * details. A non-retryable error is defined as one that is unlikely to be fixed by immediately
     * retrying the operation.
     * <p>
     * Only the following values of |GMSPlacesErrorCode| are retryable:
     * <ul>
     * <li>kGMSPlacesNetworkError
     * <li>kGMSPlacesServerError
     * <li>kGMSPlacesInternalError
     * </ul>
     * All other error codes are non-retryable.
     * @param resultsController The |GMSAutocompleteResultsViewController| that generated the event.
     * @param error The |NSError| that was returned.
     */
    public func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }

    /**
     * Called when a place has been selected from the available autocomplete predictions.
     * @param resultsController The |GMSAutocompleteResultsViewController| that generated the event.
     * @param place The |GMSPlace| that was returned.
     */
    public func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        searchController?.isActive = false
        // Do something with the selected place.
        print("Place name: ", place.name)
        print("Place address: ", place.formattedAddress)
        print("Place attributions: ", place.attributions)
    }

    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(forResultsController resultsController: GMSAutocompleteResultsViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
}




