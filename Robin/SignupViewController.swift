//
//  SignupViewController.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 9/29/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import UIKit
import Firebase

class SignupViewController: UIViewController {

    @IBOutlet weak var txtNome: UITextField!
    @IBOutlet weak var txtSobrenome: UITextField!
    
    @IBOutlet weak var txtEmail: UITextField!
    @IBOutlet weak var txtSenha: UITextField!

    @IBOutlet weak var txtCEP: UITextField!
    @IBOutlet weak var txtNumero: UITextField!
    
    var ref: FIRDatabaseReference!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround() 
        
        configureDatabase()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func configureDatabase() {
        ref = FIRDatabase.database().reference()
    }
    
    
    @IBAction func signup(_ sender: AnyObject) {
        
        guard let email = txtEmail.text, let password = txtSenha.text else { return }
        
        FIRAuth.auth()?.createUser(withEmail: email, password: password, completion: { (user, error) -> Void in
            
            if let usuario = user{
                let id = usuario.uid
                let nome = self.txtNome.text!
                let sobrenome = self.txtSobrenome.text!
                let cep = self.txtCEP.text!
                let numero = self.txtNumero.text!
                
                let endereco = [
                    "cep": cep,
                    "numero": numero
                ]
                
                let config = [
                    "raio": 5,
                    "notificacoes": true
                ] as [String : Any]
                
                let data = [
                    "nome": nome,
                    "sobrenome": sobrenome,
                    "email": email
                ]
                
                self.ref.child("usuarios").child(id).setValue(data)
                self.ref.child("usuarios").child(id).child("endereco").setValue(endereco)
                self.ref.child("usuarios").child(id).child("configuracoes").setValue(config)
                
                let alert = UIAlertController(title: "Cadastro", message: "Cadastro realizado com sucesso!", preferredStyle: UIAlertControllerStyle.alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { action in
                    self.performSegue(withIdentifier: "backLoginSegue", sender: self)
                }))
                self.present(alert, animated: true, completion: nil)

            }
            
            
        })
        
        
    }
    
    
    @IBAction func voltar(_ sender: AnyObject) {
        performSegue(withIdentifier: "backLoginSegue", sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
