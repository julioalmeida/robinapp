
import Foundation



public func timeAgoSince(_ date: Date) -> String {
    
    let calendar = Calendar.current
    let now = Date()
    let unitFlags: NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfYear, .month, .year]
    let components = (calendar as NSCalendar).components(unitFlags, from: date, to: now, options: [])
    
    if components.year! >= 2 {
        return "\(components.year!) anos atrás"
    }
    
    if components.year! >= 1 {
        return "ano passado"
    }
    
    if components.month! >= 2 {
        return "\(components.month) meses atrás"
    }
    
    if components.month! >= 1 {
        return "mês passado"
    }
    
    if components.weekOfYear! >= 2 {
        return "\(components.weekOfYear!) semanas atrás"
    }
    
    if components.weekOfYear! >= 1 {
        return "semana passada"
    }
    
    if components.day! >= 2 {
        return "\(components.day) dias atrás"
    }
    
    if components.day! >= 1 {
        return "ontem"
    }
    
    if components.hour! >= 2 {
        return "\(components.hour!) horas atrás"
    }
    
    if components.hour! >= 1 {
        return "1 hora atrás"
    }
    
    if components.minute! >= 2 {
        return "\(components.minute!) minutos atrás"
    }
    
    if components.minute! >= 1 {
        return "1 minuto atrás"
    }
    
    if components.second! >= 3 {
        return "\(components.second!) segundos atrás"
    }
    
    return "agora"
    
}
