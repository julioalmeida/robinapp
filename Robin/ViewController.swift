//
//  ViewController.swift
//  Robin
//
//  Created by Julio Cesar Almeida on 9/29/16.
//  Copyright © 2016 Robin App. All rights reserved.
//

import UIKit

import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var inputEmail: UITextField!
    @IBOutlet weak var inputPassword: UITextField!
    @IBOutlet weak var lblError: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.hideKeyboardWhenTappedAround()
        
        if let user = FIRAuth.auth()?.currentUser {
            self.signedIn(user: user)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func login(_ sender: AnyObject) {
        self.lblError.text = ""
        guard let email = inputEmail.text, let password = inputPassword.text else { return }
        FIRAuth.auth()?.signIn(withEmail: email, password: password, completion: { (user, error) in
            if let error = error {
                self.lblError.text = error.localizedDescription
                return
            }
            self.signedIn(user: user!)
        })

    }
    
    func signedIn(user: FIRUser) {
        
        let ref = FIRDatabase.database().reference()
        if let uid = user.uid as? String{
            ref.child("usuarios").child(uid).observeSingleEvent(of: .value, with: { snapshot in
                if let value = snapshot.value as? NSDictionary{

                if let nome = value["nome"] as? String{
                    if let sobrenome = value["sobrenome"] as? String{
                        AppState.sharedInstance.signedIn = true
                        AppState.sharedInstance.uid = uid
                        if let raio = value["configuracoes"] as? [String: Any]{
                            AppState.sharedInstance.raio = raio["raio"] as? Int
                        }else{
                            AppState.sharedInstance.raio = 5
                        }
                        AppState.sharedInstance.displayName = nome + " " + sobrenome
                        self.performSegue(withIdentifier: "segueLogin", sender: self)
                    }
                }
                }
            }){ (error) in
                print(error.localizedDescription)
            }
        }
        
    }

}

